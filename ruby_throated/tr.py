from trello import TrelloClient
import requests

from dotenv import load_dotenv
import os

load_dotenv()

env = dict(os.environ)

def fetch_link_cards(board, list_id):
    """Fetch all link cards from a Trello board and list."""
    link_cards = []
    try:
        for card in board.get_list(list_id).list_cards():
            if get_card_content(card) is '':
                continue
            if env['LABEL_ID'] not in card.idLabels:
                link_cards.append(card)
    except requests.exceptions.HTTPError as e:
        print(f"Error fetching link cards from list {list_id}: {e}")
        return []
    return link_cards


def get_client(api_key, api_secret, token, token_secret):
    """Get a Trello client."""
    try:
        return TrelloClient(api_key=api_key, api_secret=api_secret,token=token, token_secret=token_secret)
    except Exception as e:
        print(f"Error getting client {e}")
        return None

def is_link_card(card):
    """Return True if the card is a link card."""
    if card.name is not None and card.name.startswith("http"):
        return True
    return False


def has_link_card(card):
    """Return True if the card is a link card."""
    if card.description is not None and card.description.startswith("http"):
        return True
    return False

def get_card_content(card):
    """Return the content of the card."""

    if is_link_card(card) is True:
        return card.name
    elif has_link_card(card) is True:
        return card.description
    else:
        return ''
