from bs4 import BeautifulSoup

def test_strip_html():
    html = '<html><body><p>Hello, world!</p></body></html>'
    soup = BeautifulSoup(html, 'html.parser')
    text = soup.get_text()
    assert text == 'Hello, world!'
