import llm
import beautifulsoup
import tr
from dotenv import load_dotenv
import os
import pygithub
load_dotenv()
import prompts

env = dict(os.environ)

"""Main module."""
def main(board, list, api_key=env['API_KEY'], api_secret=env['API_SECRET']):
    """Call the trello cards, process it and return the result."""

    # Get the Trello client
    client = tr.get_client(env['API_KEY'], env['API_SECRET'],env['TRELLO_TOKEN'],env['TRELLO_TOKEN_SECRET'])

    # Get the board
    board = client.get_board(env['BOARD_ID'])

    # Get the link cards
    link_cards = tr.fetch_link_cards(board, env['LIST_ID'])

    n = 0
    # Process the link cards
    for link_card in link_cards:
        # Get the link card content
        link_card_content = tr.get_card_content(link_card)

        url = beautifulsoup.extract_url(link_card_content)

        if url.startswith('https://twitter.com'):
            continue

        stripped_html = beautifulsoup.strip_html(url)
        # Process the link card content
        link_card_content_processed = llm.process_link_card(
            env['PROMPT'],
            stripped_html
        )
        # print(stripped_html)
        print(link_card_content_processed)
        # Update the link card
        link_card.set_description(url +'\n'+ link_card_content_processed)
        link_card.add_label(client.get_label(env['LABEL_ID'], env['BOARD_ID']))
        link_card.set_name(beautifulsoup.extract_title(url) + ' - ' + url)
        n+=1
        if n > 5:
            break


"""update issues module."""
def update_issues_with_llm(project_id):
    """Call the github issues, process it and return the result."""

    # Get the link cards
    issues = pygithub.fetch_issues(project_id)

    n = 0
    # Process the link cards
    for issue in issues:
        # Get the link card content
        issue_content = pygithub.get_issue_content(issue)

        print(issue_content)
        issue_content_processed = llm.process_link_card(
            prompts.act_as_headhunter(),
            prompts.get_job_prompt(issue_content)
        )
        # Update the link card
        pygithub.update_issue(issue, issue_content, issue_content_processed)
        n+=1
        if n > 2:
            break
