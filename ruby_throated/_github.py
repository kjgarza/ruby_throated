import os
import requests
from dotenv import load_dotenv

load_dotenv()

env = dict(os.environ)

def fetch_link_cards(project_id):
    """Fetch all link cards from a GitHub project."""
    link_cards = []
    try:
        query = """
        query {
          node(id: "%s") {
            ... on Project {
              name
            items(first: 100) {
                nodes {
                id
                note
                content {
                    ... on Issue {
                    title
                    body
                    url
                    }
                }
                }
            }
            }
          }
        }
        """ % (project_id)
        headers = {
            'Authorization': f"Bearer {env['GITHUB_TOKEN']}",
            'Accept': 'application/vnd.github.inertia-preview+json'
        }
        response = requests.post('https://api.github.com/graphql', json={'query': query}, headers=headers)
        response.raise_for_status()
        data = response.json()['data']['node']['items']['nodes']
        for card in data:
            if is_link_card(card):
                link_cards.append(card)
    except Exception as e:
        print(f"Error fetching link cards from column {project_id}: {e}")
        return []
    return link_cards

def is_link_card(card):
    """Return True if the card is a link card."""
    if 'content' in card and card['content'] is not None:
        return card['content']['url'] is not None and card['content']['url'].startswith('http')
    elif 'note' in card and card['note'] is not None:
        return card['note'].startswith('http')
    else:
        return False

def get_card_content(card):
    """Return the content of the card."""

    if 'content' in card and card['content'] is not None:
        return card['content']['url']
    elif 'note' in card and card['note'] is not None:
        return card['note']
    else:
        return ''
