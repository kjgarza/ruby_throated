# """Console script for ruby_throated."""
# import sys
# import click


# @click.command()
# def main(args=None):
#     """Console script for ruby_throated."""
#     click.echo("Replace this message by putting your code into "
#                "ruby_throated.cli.main")
#     click.echo("See click documentation at https://click.palletsprojects.com/")
#     return 0


# if __name__ == "__main__":
#     sys.exit(main())  # pragma: no cover

import sys
import click
import ruby_throated


@click.group()
def cli():
    pass


@cli.command()
@click.option('--board', default='Learning', help='Trello board ID')
@click.option('--list', default='ToDo', help='Trello list ID')
@click.option('--api-key', help='Trello API key')
@click.option('--api-secret', help='Trello API secret')
def trello(board, list, api_key, api_secret):
    """Process link cards from a Trello board and list."""
    ruby_throated.main(board, list, api_key, api_secret)

@cli.command()
def github():
    """Process link cards from a Trello board and list."""
    ruby_throated.update_issues_with_llm('2023-project')

if __name__ == "__main__":
    sys.exit(cli())

