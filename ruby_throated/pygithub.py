import os
import requests
from dotenv import load_dotenv

load_dotenv()

env = dict(os.environ)

from github import Github
# Authentication is defined via github.Auth
from github import Auth

# using an access token
auth = Auth.Token(env["GITHUB_TOKEN"])

# Public Web Github
g = Github(auth=auth)


def fetch_issues(project_id):
    """Fetch all link cards from a GitHub project."""
    repo = g.get_user().get_repo("2023-project")
    issues = [issue for issue in repo.get_issues(state="open") if 'processed' not in list(map(lambda x: x.name , issue.labels))]
    issues_flt = [x for x in issues if x.body is not None and len(x.body) > 100 ]
    return issues_flt

def is_linked_issue(card):
    """Return True if the issue is has a link"""
    return card.body.startswith('http')


def get_issue_content(issue):
    """Return the content of the card."""
    return issue.body

def add_label(issue, label):
    """If issue title contains word add label"""
    if label in issue.title:
        issue.add_to_labels(label)

def update_issue(issue, orignal, processed):
    """Update the issue with the new body"""
    new_body =f"""
        {orignal}
        \n
        ----------------
        {processed}
        """
    add_label(issue, 'senior')
    add_label(issue, 'manager')
    issue.add_to_labels('processed')
    issue.edit(body=new_body)
