def get_job_prompt(job_description):
    return f"""
    ##JOB-DESCRIPTION

    {job_description}

    ##END-OF-JOB-DESCRIPTION


    ##RESUME

    ###Experience
    ####PRODUCT DESIGNER, DataCite, 2020 - current

        ●	Leveraged Design Sprints and use of lean UX to institute DesignOps procedures at DataCite, enhancing project delivery efficiency and boosting stakeholders satisfaction through more punctual and regular product updates.
        ●	Led user experience research for a dashboard through focus groups, interviews, and walkthroughs of wireframes in Figma. Findings reduced lead time and increased data completeness.
        ●	I led the research and design of an alternative usage processing pipeline, implementing structural changes to the system that I designed. These changes resulted in a 3x increase in sign-ins.
        ●	Successfully conceptualised, designed, and launched the DataCite Design system, aimed to further streamline the design-to-production process and expected overall development efficiency.
        ●	Built collaborative relationships with a cross-functional team of 5 engineers, external stakeholders, and DataCite leaders, strengthened the team's synergy.
        ●	Over the last 2 years, I collaboratively  crafted and refined the product vision, strategy, and roadmap for DataCite, aligned with the organisation's mission and grant's goals, enhancing collaboration and productivity.
        ●	Built collaborative relationships with a cross-functional team of 5 developers, external stakeholders, and DataCite leaders, strengthen the team's synergy.
        ●	Spearheaded the feature design and information architecture of DataCite’s primary web platform, using User Journeys, Personas, User Stories, Stakeholder Mappings, WireFraming techniques, and Figma for design, leading to an enhancement in user experience and an increase in user interaction. This approach also resulted in a decrease in user-reported issues.
        ●	Facilitated company-wide retrospectives, Light Decision Jams and Led design sprints for multiple services efficiently aligning perspectives from a diversity of stakeholders.

    ####FULL STACK DEVELOPER, DataCite, 2016-2020

        ●	Implemented a Python package to convert metadata using OpenAI's GPT4. The package marked a radical change in the way metadata translation is done across systems and organisation. I presented this work in EU EOSC Workshop.
        ●	Built, launched and maintained a Rails GraphQL API for scholarly metadata persisted in ElasticSearch. The API also functioned as a gateway to another two REST APIs.
        ●	Engineered, launched and maintained a robust ReactJS SPA using TypeScript with a GraphQL backend on AWS and ElasticSearch.
        ●	Architected, launched and maintained a  Rails REST API, SQS for queuing, S3 and MySQL for data storage.
        ●	Designed,  and implemented a Flask GraphQL API in python to serve as a Gateway to multiple EOSC Graph APIs

    ####OPEN SCIENCE GRADUATE RESEARCHER, University of Manchester, UK, 2014-2016

        ●	Investigated the utilisation of persistent identifiers for research data identification, thereby expediting the adoption of PIDs by UK nodes through the provision of a comprehensive report.

    #### PHP DEVELOPER DEVELOPER, DataMine, 2009-2011

        ●	Devised features for a ERP system using a Symfony-like framework in PHP, increasing system efficiency and significantly enhancing maintainability due to the implementation of modern programming practices. Tools: PHP, MySQL.
    ####ELECTRONICS MANUFACTURING ENGINEER, Yazaki North America, 2003-2007

        ●	Led the successful integration of three new electronic products into the SMT manufacturing line, employing extensive knowledge of ICT testing and AOI inspection, which resulted in an increase in production capacity without compromising on product quality.
        ●	Streamlined the SMT process flow employing PFMEA and Kanban methods. This optimized approach yielded a reduction in defects and an improvement in production efficiency by reducing work-in-progress and improving flow.

    ###Education
    ####PHD COMPUTER SCIENCE, The University of Manchester, UK, 2012-2016

        ●	Led the investigation of the employment of a novel choice architecture approach to integrate the captured context into the data repository design, developing features that resonated with the user base.

    ####MSC SPACECRAFT TECHNOLOGY, University College London, UK, 2007-2008
        ●	Defined design improvements of electron detectors.

    ###Published work
        ●	Garza, K. (2023). DataCite Design System is ready to be worn. DataCite. doi.org/10.5438/TPAH-AJ25
        ●	Garza, K. (2022). Refining our Thinking: How we are improving DataCite design processes. doi.org/10.5438/F03X-VR69
        ●	de Jong, M. & Garza, K. (2019). Project FREYA: Connecting Open Knowledge in the European Open Science Cloud (EOSC). doi.org/10.5281/ZENODO.3517852
        ●	Garza, K. (2013). Individual Motivations Change Activity in Online Scientific Communities. figshare. doi.org/10.6084/M9.FIGSHARE.830 404
    ###Blog
        ●	Garza, K. (2023). ParrotGPT: On the Advantages of Large Language Models Tools (AI) for Academic Metadata Schema Mapping. doi.org/10.59350/hs9k1-wn031
        ●	Garza, K. (2023). Revolutionizing Metadata Schema Mapping with ChatGPT and AI. doi.org/10.59350/b9na4-hq881
        ●	Garza, K. (2023). Academic Publishing web forms meet your demise: The unstoppable rise of large language models…. Front Matter. doi.org/10.59350/ye9qe-fm404
    ##END-OF-RESUME

    ##COVER-LETTER-DIRECTIONS
        ●	Only include the Body of the cover letter.
        ●	The cover letter is written in first person.
        ●	The Cover letter must be 4 paragraphs long
    ##END-OF-COVER-LETTER-DIRECTIONS

    ——

    """.strip()


def act_as_headhunter():
    return """
    Act as a talented Head hunter for Digital design domain. You are British and have a great command of your language. You are eloquent yet concise. Be modest.
        1	Identify keywords for skills and tools in the JOB-DESCRIPTION.
        2.  Identify potential matches for such keywords in the RESUME, either implied or inferred.
        3.  Considering the COVER-LETTER-DIRECTIONS, Write a cover letter for job in the JOB-DESCRIPTION. Based on  JOB-DESCRIPTION select the experiences, published work or blogs from the RESUME that are most fitting to the requirements of the JOB-DESCRIPTION. Use matched keywords.

    Lets go step by step.

    """.strip()

def act_as_recruiter():
    return """
        Act as a talented Head hunter for Digital design domain. You are British and have a great command of your language. You are eloquent yet concise. Be modest.
        You extreamely familiar with UX, Product Design, Software Developer, Scholalrly Open Infrastructure.
    """.strip()

def identify_expereriences():
    return """

    Based on  JOB-DESCRIPTION select the experiences, published work or blogs from the RESUME that are most fitting to the requirements of the JOB-DESCRIPTION. List them as bullet points, separated by experience.

    """.strip()
