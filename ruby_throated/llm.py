import openai
from dotenv import load_dotenv
import os

load_dotenv()

env = dict(os.environ)

def process_link_card(prompt, text):
    """Process a link card with OpenAI."""
    openai.api_key = env['OPEN_AI_KEY']
    try:
        result = openai.ChatCompletion.create(
            model="gpt-4",
            max_tokens=1200,
            temperature=0.7,
            top_p=1,
            frequency_penalty=0,
            presence_penalty=0,
            messages=[
                {"role": "system", "content": prompt},
                {"role": "user", "content": text}
            ]
        )
    except Exception as e:
        print(f"Error getting client {e}")
        result = ''

    return result.choices[0].message.content
