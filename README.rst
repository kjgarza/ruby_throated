=============
ruby_throated
=============


.. image:: https://img.shields.io/pypi/v/ruby_throated.svg
        :target: https://pypi.python.org/pypi/ruby_throated

.. image:: https://img.shields.io/travis/kjgarza/ruby_throated.svg
        :target: https://travis-ci.com/kjgarza/ruby_throated

.. image:: https://readthedocs.org/projects/ruby-throated/badge/?version=latest
        :target: https://ruby-throated.readthedocs.io/en/latest/?version=latest
        :alt: Documentation Status




Python Boilerplate contains all the boilerplate you need to create a Python package.


* Free software: MIT license
* Documentation: https://ruby-throated.readthedocs.io.


Features
--------

* TODO

Credits
-------

This package was created with Cookiecutter_ and the `audreyr/cookiecutter-pypackage`_ project template.

.. _Cookiecutter: https://github.com/audreyr/cookiecutter
.. _`audreyr/cookiecutter-pypackage`: https://github.com/audreyr/cookiecutter-pypackage
