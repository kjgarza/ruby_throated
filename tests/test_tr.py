import tr

def test_fetch_link_cards():
    client = tr.get_client(api_key='API_KEY', api_secret='API_SECRET')
    board = client.get_board('BOARD_ID')
    link_cards = tr.fetch_link_cards(board, 'LIST_ID')
    assert len(link_cards) > 0
