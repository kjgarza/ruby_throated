.. highlight:: shell

============
Installation
============


Stable release
--------------

To install ruby_throated, run this command in your terminal:

.. code-block:: console

    $ pip install ruby_throated

This is the preferred method to install ruby_throated, as it will always install the most recent stable release.

If you don't have `pip`_ installed, this `Python installation guide`_ can guide
you through the process.

.. _pip: https://pip.pypa.io
.. _Python installation guide: http://docs.python-guide.org/en/latest/starting/installation/


From sources
------------

The sources for ruby_throated can be downloaded from the `Github repo`_.

You can either clone the public repository:

.. code-block:: console

    $ git clone git://github.com/kjgarza/ruby_throated

Or download the `tarball`_:

.. code-block:: console

    $ curl -OJL https://github.com/kjgarza/ruby_throated/tarball/master

Once you have a copy of the source, you can install it with:

.. code-block:: console

    $ python setup.py install


.. _Github repo: https://github.com/kjgarza/ruby_throated
.. _tarball: https://github.com/kjgarza/ruby_throated/tarball/master
