import openai

def test_process_link_card():
    openai.api_key = 'API_KEY'
    prompt = "Summarize the content and provide the main 5 takeaways."
    text = "This is a test link card."
    result = openai.Completion.create(
        engine='davinci',
        prompt=prompt,
        max_tokens=1024,
        n=1,
        stop=None,
        temperature=0.5,
        frequency_penalty=0,
        presence_penalty=0,
        inputs={'text': text}
    )
    assert len(result.choices) == 1
