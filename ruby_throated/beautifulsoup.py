from bs4 import BeautifulSoup
import requests

def strip_html(url):
    """Scrape a URL and strip its HTML."""
    headers = { 'content-type': 'text/html; charset=UTF-8' }
    response = requests.get(url, headers=headers)

    if not response.ok:
        return ''
    try:
        soup = BeautifulSoup(response.content, 'html.parser')
        body = soup.find('body')
        return chop_text(body.get_text(),2000)
    except Exception:
        return ''

def extract_title(url):
    """Scrape a URL and extract its title."""
    headers = { 'content-type': 'text/html; charset=UTF-8' }
    response = requests.get(url, headers=headers)

    if not response.ok:
        return ''
    try:
        soup = BeautifulSoup(response.content, 'html.parser')
        return chop_text(soup.title.string, 80)
    except Exception:
        return ''

def extract_url(text):
    """Extract the first URL from a text. it must be a http or https url"""
    if text is None:
        return ''
    words = text.split(' ')
    for word in words:
        if word.startswith('http'):
            return word
    return ''

def chop_text(text, max_length):
    """Chop a text to a maximum length."""
    if len(text) > max_length:
        return text[:max_length]
    return text

